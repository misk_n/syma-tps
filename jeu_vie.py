import random


class Agent:
    x = 0
    y = 0
    state = 0
    futur = state

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __str__(self):
        return str(self.state)

    def def_futur(self, matrix):
        alive = 0
        for i in range(-1, 2):
            for j in range(-1, 2):
                if i == 0 and j == 0:
                    continue
                x = self.x + i
                y = self.y + j
                if x < 0 or x >= len(matrix):
                    continue
                if y < 0 or y >= len(matrix[0]):
                    continue
                alive += matrix[x][y].state

        if alive < 2 and self.state == 1:
            self.futur = 0
        elif alive > 3 and self.state == 1:
            self.futur = 0
        elif alive == 3 and self.state == 0:
            self.futur = 1
        else:
            self.futur = self.state
        # adj: vivante entouree <2 cellules vivantes => meurt
        # adj: vivante entouree >3 cellules vivantes => meurt
        # adj: morte entouree 3 cellules vivantes => revit


def map_matrix(matrix, f):
    for x in range(len(matrix)):
        for y in range(len(matrix[0])):
            f(matrix[x][y])


def init_matrix(matrix, n, m):
    for i in range(n):
        row = []
        for j in range(m):
            row += [Agent(i, j)]
        matrix.append(row)


def print_matrix(matrix):
    for x in range(len(matrix)):
        string = ""
        for y in range(len(matrix[0])):
            string += str(matrix[x][y]) + " "
        print(string)


def random_init_agent(matrix, num):
    for i in range(num):
        x = random.randint(0, len(matrix) - 1)
        y = random.randint(0, len(matrix[0]) - 1)
        matrix[x][y].state = 1


def turn(matrix):
    def call_update(agent):
        agent.def_futur(matrix)
    map_matrix(matrix, call_update)

    def define_state(agent):
        agent.state = agent.futur
    map_matrix(matrix, define_state)


def main():
    n = 6
    m = 8
    turns = 5
    matrix = []
    init_matrix(matrix, n, m)
    random_init_agent(matrix, 10)
    for i in range(turns):
        turn(matrix)
        print_matrix(matrix)
        print()


main()